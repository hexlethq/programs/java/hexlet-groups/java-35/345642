package exercise;

import java.util.Arrays;
import java.util.stream.Collectors;

class App {

    public static String getForwardedVariables(String conf) {
        String[] confAr = conf.split("\n");
        return Arrays.stream(confAr)
                .filter(str -> str.startsWith("environment="))
                .map(str -> str.replaceAll("environment=\"", "")
                        .split(","))
                .flatMap(Arrays::stream)
                .filter(str -> str.startsWith("X_FORWARDED_"))
                .map(str -> str.replaceAll("X_FORWARDED_", "")
                        .replaceAll("\\[", "")
                        .replaceAll("]", "")
                        .replaceAll("\"", ""))
                .collect(Collectors.joining(","));
    }
}
