package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static String buildList (String[] arrStr) {
        if (arrStr.length == 0) {
            return "";
        }
        StringBuilder htmlList = new StringBuilder("<ul>" + "\n");
        for (String elements : arrStr) {
            htmlList.append("  ").append("<li>").append(elements).append("</li>").append("\n");
        }
        return (htmlList.append("</ul>")).toString();
    }

    public static String getUsersByYear(String[][] arrOfUsersAndYear, int year) {
        String strYear = Integer.toString(year);
        StringBuilder userYear = new StringBuilder();
        String[] arrUser = new String[arrOfUsersAndYear.length];
        int indexForArrUser = 0;
        for (int i = 0; i < arrOfUsersAndYear.length; i++) {
            userYear.append(arrOfUsersAndYear[i][1]).delete(4, arrOfUsersAndYear[i][1].length());
            if (userYear.toString().equals(strYear)) {
                arrUser[indexForArrUser] = arrOfUsersAndYear[i][0];
                indexForArrUser += 1;
            }
            userYear.delete(0, userYear.length());
        }
        int indexOfNull = 0;
        for (int i = 0; i < arrUser.length; i++) {
            if (arrUser[i] == null) {
                indexOfNull = i;
                break;
            }
        }
        String[] arrUserCopy = Arrays.copyOf(arrUser, indexOfNull);
        return buildList(arrUserCopy);
    }
    // END
}
