// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;

public class App {

    public static double[] getMidpointOfSegment(double[][] segment) {
        double[] midPointOfSegment = new double[2];
        double[] beginPoint = Segment.getBeginPoint(segment);
        double[] endPoint = Segment.getEndPoint(segment);
        midPointOfSegment[0] = (Point.getX(beginPoint) + Point.getX(endPoint)) / 2;
        midPointOfSegment[1] = (Point.getY(beginPoint) + Point.getY(endPoint)) / 2;
        return midPointOfSegment;
    }

    public static double[][] reverse(double[][] segment) {
        double[] beginPoint = Segment.getBeginPoint(segment);
        double[] endPoint = Segment.getEndPoint(segment);
        double[][] reverseSegment = {
                                    {Point.getX(endPoint), Point.getY(endPoint)},
                                    {Point.getX(beginPoint), Point.getY(beginPoint)}
                                    };
        return reverseSegment;
    }

    public static boolean isBelongToOneQuadrant(double[][] segment) {
        double[] beginPoint = Segment.getBeginPoint(segment);
        double[] endPoint = Segment.getEndPoint(segment);
        return (Point.getX(beginPoint) * Point.getX(endPoint) > 0) && (Point.getY(beginPoint) * Point.getY(endPoint) > 0);
    }
}
// END
