package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        int cardNumberLength = cardNumber.length();
        System.out.println("*".repeat(starsCount) + cardNumber.subSequence((cardNumberLength - 4), cardNumberLength));
        // END
    }
}
