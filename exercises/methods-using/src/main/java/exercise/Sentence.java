package exercise;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        System.out.println((sentence.startsWith("!", sentence.length() - 1)) ? sentence.toUpperCase() : sentence.toLowerCase());
        // END
    }
}
