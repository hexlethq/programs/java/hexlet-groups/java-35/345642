<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>User</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
            crossorigin="anonymous">
    </head>
    <body>
        <c:set var="id" value="${id}"/>
        <c:forEach var="user" items="${users}">
            <c:if test = '${user.get("id") == id}'>
                <table>
                    <tr>
                        <td>${user.get("firstName")} ${user.get("lastName")} ${user.get("email")}</td>
                    </tr>
                    <tr>
                        <td>
                            <a href='/users/delete?id=${id}'>Удалить пользователя</a>
                        </td>
                    </tr>
                </table>
            </c:if>
        </c:forEach>
    </body>
</html>

<!-- END -->
