package exercise;

import java.time.LocalDateTime;

import exercise.daytimes.Daytime;
import exercise.daytimes.Morning;
import exercise.daytimes.Day;
import exercise.daytimes.Evening;
import exercise.daytimes.Night;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// BEGIN
@Configuration
public class MyApplicationConfig {

    @Bean
    public Daytime getDayTime() {
        int currentTime = LocalDateTime.now().getHour();
        if (currentTime >= 6 && currentTime < 12) {
            return new Morning();
        } else if (currentTime >= 12 && currentTime < 18) {
            return new Day();
        } else if (currentTime >= 18 && currentTime < 23) {
            return new Evening();
        } else {
            return new Night();
        }
    }
}
// END
