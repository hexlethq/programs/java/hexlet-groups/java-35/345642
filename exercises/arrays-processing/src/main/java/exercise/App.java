package exercise;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] arr) {
        if (arr.length == 0) {
            return -1;
        }
        int minMax = arr[0];
        for (int arrElements : arr) {
            if (minMax > arrElements) {
                minMax = arrElements;
            }
        }
        if (minMax >= 0) {
            return -1;
        }
        int soughtIndex = 0;
        for (int i = 0; i < arr.length; i++) {
            if (minMax < arr[i] && arr[i] < 0) {
                minMax = arr[i];
                soughtIndex = i;
            }
        }
        return soughtIndex;
    }

    public static int[] getElementsLessAverage(int[] arr) {
        double sumOfElements = .0;
        int checker = 0;
        for (int arrElements : arr) {
            sumOfElements += arrElements;
            if (arrElements != 0) {
                checker += 1;
            }
        }
        double average = sumOfElements / arr.length;
        if (checker == 0) {
            return arr;
        }
        int checker2 = 0;
        for (int arrElements : arr) {
            if (arrElements <= average) {
                checker2 += 1;
            }
        }
        int[] newArr = new int[checker2];
        int j = 0;
        for (int arrElements : arr) {
            if (arrElements <= average) {
                newArr[j] = arrElements;
                j += 1;
            }
        }
        return newArr;
    }
    // END
}
