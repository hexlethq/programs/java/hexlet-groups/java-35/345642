package exercise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;


class App {

    public static LinkedHashMap<String, String> genDiff(Map<String, Object> data1, Map<String, Object> data2) {
        LinkedHashMap<String, String> resultData = new LinkedHashMap<>();
        TreeMap<String, Object> data3 = new TreeMap<>(data2);
        data3.putAll(data1);
        data3.keySet().forEach(
                key -> {
                    if(!data1.containsKey(key)) {
                        resultData.put(key, "added");
                    }
                    else if(data1.containsKey(key) && data2.containsKey(key)) {
                        if (data1.get(key).equals(data2.get(key))) {
                            resultData.put(key, "unchanged");
                        } else {
                            resultData.put(key, "changed");
                        }
                    }
                    else {
                        resultData.put(key, "deleted");
                    }
                }
                );
        return resultData;
    }

}
