package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void testTake() {
        // BEGIN
        List<Integer> numbers1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        int nElements1 = 2;
        List<Integer> expectedResult1 = new ArrayList<>(Arrays.asList(1, 2));
        List<Integer> resultNumbers1 = App.take(numbers1, nElements1);
        assertThat(resultNumbers1).isEqualTo(expectedResult1);

        List<Integer> numbers2 = new ArrayList<>(Arrays.asList(7, 3, 10));
        int nElements2 = 8;
        List<Integer> expectedResult2 = new ArrayList<>(Arrays.asList(7, 3, 10));
        List<Integer> resultNumbers2 = App.take(numbers2, nElements2);
        assertThat(resultNumbers2).isEqualTo(expectedResult2);

        List<Integer> numbers3 = new ArrayList<>();
        int nElements3 = 3;
        List<Integer> expectedResult3 = new ArrayList<>();
        List<Integer> resultNumbers3 = App.take(numbers3, nElements3);
        assertThat(resultNumbers3).isEqualTo(expectedResult3);
        // END
    }
}
