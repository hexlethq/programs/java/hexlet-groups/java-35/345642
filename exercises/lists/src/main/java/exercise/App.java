package exercise;

import java.util.ArrayList;
import java.util.List;

// BEGIN
public class App {

    public static boolean scrabble(String stringOfSymbols, String word) {
        List<Character> list = new ArrayList<>();
        for(char ch : stringOfSymbols.toCharArray()) {
            list.add(ch);
        }
        for(char ch : word.toLowerCase().toCharArray()) {
            if (list.contains(ch)) {
                list.remove(Character.valueOf(ch));
            } else {
                return false;
            }
        }
        return true;
    }
}
//END
