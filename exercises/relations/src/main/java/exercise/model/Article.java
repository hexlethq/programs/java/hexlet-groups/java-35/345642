package exercise.model;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.GeneratedValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.GenerationType;

// BEGIN
@Getter
@Setter
@Entity
public class Article {

    // Первичный автогенерируемый ключ
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    // Поле может иметь большое содержимое, например текст
    @Lob
    private String body;

    // В категории может быть множество статей
    // Но статья принадлежит одной конкретной категории
    @ManyToOne
    private Category category;
}
// END
