package exercise;

import java.util.Arrays;


// BEGIN
public class Kennel {
    static StringBuilder kennelOfPuppies = new StringBuilder();

   public static StringBuilder addPuppy(String[] arrPuppy) {
       return kennelOfPuppies.append(Arrays.toString(arrPuppy)).append(", ");
   }

   public static StringBuilder addSomePuppies(String[][] arrPuppies) {
       StringBuilder strArrPuppies = new StringBuilder(Arrays.deepToString(arrPuppies));
       strArrPuppies.deleteCharAt(0).deleteCharAt(strArrPuppies.length() - 1);
       return kennelOfPuppies.append(strArrPuppies).append(", ");
   }

   public static int getPuppyCount() {
       int countOfPuppies = 0;
       if (kennelOfPuppies.length() == 0)
           return countOfPuppies;
       for (int i = 0; i < kennelOfPuppies.length() - 1; i++) {
           if (kennelOfPuppies.charAt(i) == ']' && kennelOfPuppies.charAt(i + 1) == ',') {
               countOfPuppies += 1;
           }
       }
       return countOfPuppies;
    }

    public static boolean isContainPuppy(String namePuppy) {
        return kennelOfPuppies.lastIndexOf(namePuppy) != -1;
    }

    public static String[][] getAllPuppies() {
        String strKennelOfPuppies = kennelOfPuppies.toString().replaceAll("\\[", "").replaceAll("]", "");
        String[] arrKennelOfPuppies = strKennelOfPuppies.split(", ");
        String[][] soughtArrKennelOfPuppies = new String[getPuppyCount()][2];
        int jIndex = 0;
        for (int i = 0; i < getPuppyCount(); i++) {
            soughtArrKennelOfPuppies[i][0] = arrKennelOfPuppies[jIndex];
            soughtArrKennelOfPuppies[i][1] = arrKennelOfPuppies[jIndex + 1];
            jIndex += 2;
        }
        return soughtArrKennelOfPuppies;
    }

    public static String[] getNamesByBreed(String breed) {
       StringBuilder strBreed = new StringBuilder();
       for (int i = 0; i < getPuppyCount(); i++) {
           if (getAllPuppies()[i][1].equals(breed)) {
               strBreed.append(getAllPuppies()[i][0]).append(", ");
           }
       }
       return strBreed.toString().split(", ");
    }

    public static StringBuilder resetKennel() {
       return kennelOfPuppies.delete(0, kennelOfPuppies.length());
    }

    public static boolean removePuppy(String namePuppy) {
       String removeNamePuppy = "";
       String removeBreedPuppy = "";
       for (int i = 0; i < getPuppyCount(); i++) {
           if (getAllPuppies()[i][0].equals(namePuppy)) {
               removeNamePuppy = getAllPuppies()[i][0];
               removeBreedPuppy = getAllPuppies()[i][1];
           }
       }
       if (removeNamePuppy.equals("")) {
           return false;
       }
       String removeNameAndBreedPuppy = "\\[" + removeNamePuppy + ", " + removeBreedPuppy + "], ";
       String removeKennelOfPuppies = kennelOfPuppies.toString().replaceAll(removeNameAndBreedPuppy, "");
       kennelOfPuppies.delete(0, kennelOfPuppies.length()).append(removeKennelOfPuppies);
       return true;
    }
}
// END