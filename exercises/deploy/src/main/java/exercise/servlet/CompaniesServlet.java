package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;
import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        // BEGIN
        String search = request.getParameter("search");
        PrintWriter out = response.getWriter();
        if (search == null || search.equals("")) {
            getCompanies().forEach(out::println);
        } else {
            List<String> companies = new ArrayList<>();
            getCompanies().stream()
                    .filter(
                            company -> company.toString().contains(search)
                    ).forEach(
                    company -> companies.add(company.toString())
            );
            if (companies.isEmpty()) {
                out.println("Companies not found");
            } else {
                companies.forEach(out::println);
            }
        }
        // END
    }
}
