package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// BEGIN
public class Validator {

    public static List<String> validate(Address address) {
        List<String> notValidFields = new ArrayList<>();
        for (Field field : address.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                if ((field.getDeclaredAnnotation(NotNull.class) != null) && (field.get(address) == null)) {
                    notValidFields.add(field.getName());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return notValidFields;
    }

    public static Map<String, List<String>> advancedValidate(Address address) {
        String strNull = "can not be null";
        String strLength = "length less then ";
        Map<String, List<String>> notValidFields = new HashMap<>();
        validate(address).forEach((e) -> notValidFields.put(e, List.of(strNull)));
        for (Field field : address.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                MinLength minLength = field.getDeclaredAnnotation(MinLength.class);
                if ((minLength != null) && (field.get(address) != null)
                        && (field.get(address).toString().length() < minLength.minLength())) {
                    notValidFields.put(field.getName(), List.of(strLength + minLength.minLength()));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return notValidFields;
    }
}
// END
