package exercise;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Map;

class ValidationTest {

    @Test
    void testValidate() {
        Address address1 = new Address("Russia", "Ufa", "Lenina", "54", null);
        List<String> result1 = Validator.validate(address1);
        List<String> expected1 = List.of();
        assertThat(result1).isEqualTo(expected1);

        Address address2 = new Address(null, "London", "1-st street", "5", "1");
        List<String> result2 = Validator.validate(address2);
        List<String> expected2 = List.of("country");
        assertThat(result2).isEqualTo(expected2);

        Address address3 = new Address("USA", null, null, null, "1");
        List<String> result3 = Validator.validate(address3);
        List<String> expected3 = List.of("city", "street", "houseNumber");
        assertThat(result3).isEqualTo(expected3);
    }

    // BEGIN
    @Test
    @DisplayName("Annotation parameter minLength of class 'Address' declared 5 to field 'country'\n"
            + "Annotation parameter minLength of class 'Address' is default to field 'street'")
    void testAdvancedValidate() {
        Address address1 = new Address("Russia", "Ufa", "Lenina", "54", null);
        Map<String, List<String>> result1 = Validator.advancedValidate(address1);
        assertThat(result1).isEqualTo(Map.of());

        Address address2 = new Address(null, "London", "1", "5", "1");
        Map<String, List<String>> result2 = Validator.advancedValidate(address2);
        assertThat(result2.containsKey("country")).isEqualTo(true);
        assertThat(result2.get("country")).isEqualTo(List.of("can not be null"));
        assertThat(result2.containsKey("street")).isEqualTo(true);
        assertThat(result2.get("street")).isEqualTo(List.of("length less then 3"));
        assertThat(result2.containsKey("city")).isEqualTo(false);
        assertThat(result2.containsKey("houseNumber")).isEqualTo(false);


        Address address3 = new Address("USA", null, "Le", "4", "1");
        Map<String, List<String>> result3 = Validator.advancedValidate(address3);
        assertThat(result3.containsKey("country")).isEqualTo(true);
        assertThat(result3.get("country")).isEqualTo(List.of("length less then 5"));
        assertThat(result3.containsKey("city")).isEqualTo(true);
        assertThat(result3.get("city")).isEqualTo(List.of("can not be null"));
        assertThat(result3.containsKey("street")).isEqualTo(true);
        assertThat(result3.get("street")).isEqualTo(List.of("length less then 3"));
        assertThat(result2.containsKey("houseNumber")).isEqualTo(false);
        assertThat(result2.containsKey("city")).isEqualTo(false);
        assertThat(result2.containsKey("flatNumber")).isEqualTo(false);
    }
    // END
}
