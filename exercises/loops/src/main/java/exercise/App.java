package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        String newStr = "";
        if (str.charAt(0) != ' ') {
            newStr += str.charAt(0);
        }
        for (int i = 0; i < str.length() - 1; i++) {
            if ((str.charAt(i) == ' ') && (str.charAt(i + 1) != ' ')) {
                newStr += str.charAt(i + 1);
            }
        } return newStr.toUpperCase();
    }
    // END
}
