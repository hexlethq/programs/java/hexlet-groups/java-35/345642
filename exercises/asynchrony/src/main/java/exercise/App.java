package exercise;

import java.io.File;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

class App {

    // BEGIN
    public static CompletableFuture<String> unionFiles(
            String filePath1,
            String filePath2,
            String unionFilePath) throws ExecutionException, InterruptedException {

        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            try {
                return Files.readString(Path.of(filePath1));
            } catch (IOException e) {
                System.out.println("Exception: " + e);
                return null;
            }
        });
        future1.get();

        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            try {
                return Files.readString(Path.of(filePath2));
            } catch (IOException e) {
                System.out.println("Exception: " + e);
                return null;
            }
        });
        future2.get();

        return future1.thenCombine(future2, (content1, content2) -> {
            try {
                if (!Files.exists(Path.of(unionFilePath))) {
                    Files.createFile(Path.of(unionFilePath));
                }
                Files.writeString(Path.of(unionFilePath), content1.trim() + " " + content2.trim());
            } catch (IOException e) {
                System.out.println("Exception: " + e);
            }
            return content1 + content2;
        }).exceptionally(e -> {
            System.out.println("Exception: " + e);
            return null;
        });
    }

    // Самостоятельная работа
    public static CompletableFuture<Long> getDirectorySize(String dirPath) {
        return CompletableFuture.supplyAsync(() -> {
            File[] files = Path.of(dirPath).toFile().listFiles();
            long size = 0;
            if (files == null) {
                throw new NullPointerException();
            } else {
                for (File file : files) {
                    if (file.isFile()) {
                        size += file.length();
                    }
                }
                return size;
            }
        }).exceptionally(e -> {
            System.out.println("Exception: NoSuchDirectoryException " + e);
            return null;
        });
    }
    // END

    public static void main(String[] args) throws Exception {
        // BEGIN
        App.unionFiles(
                "src/main/resources/file1.txt",
                "src/main/resources/file2.txt",
                "src/main/resources/union_file.txt"
        );

        App.getDirectorySize("src/main1/resources").get();
        // END
    }
}

