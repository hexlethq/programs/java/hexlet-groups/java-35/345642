package exercise;

import java.util.Arrays;

class Point {
    // BEGIN
    public static int[] makePoint(int xCoordinate, int yCoordinate) {
        return new int[]{xCoordinate, yCoordinate};
    }

    public static int getX (int[] arrayPointX) {
        return arrayPointX[0]; // X coordinate
    }

    public static int getY (int[] arrayPointY) {
        return arrayPointY[1]; // Y coordinate
    }

    public static String pointToString (int[] arrayPointToString) {
        String newStr = Arrays.toString(arrayPointToString);
        return "(" + newStr.substring(1, newStr.length() - 1) + ")";
    }

    public static int getQuadrant (int[] arrayPointQuadrant) {
        if (arrayPointQuadrant[0] > 0 && arrayPointQuadrant[1] > 0) {
            return 1;
        } else if ((arrayPointQuadrant[0] < 0 && arrayPointQuadrant[1] > 0)) {
            return 2;
        } else if ((arrayPointQuadrant[0] < 0 && arrayPointQuadrant[1] < 0)) {
            return 3;
        } else if ((arrayPointQuadrant[0] > 0 && arrayPointQuadrant[1] < 0)) {
            return 4;
        } else {
            return 0;
        }
    }
    // END
}
