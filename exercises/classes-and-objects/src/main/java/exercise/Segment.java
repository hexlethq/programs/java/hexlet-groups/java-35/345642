package exercise;

// BEGIN
public class Segment {

    private final Point beginPoint;
    private final Point endPoint;

    public Segment(Point beginPoint, Point endPoint) {
        this.beginPoint = beginPoint;
        this.endPoint = endPoint;
    }

    public Point getBeginPoint() {
        return this.beginPoint;
    }

    public Point getEndPoint() {
        return this.endPoint;
    }

    public Point getMidPoint() {
        int midPointX = (this.beginPoint.getX() + this.endPoint.getX()) / 2;
        int midPointY = (this.beginPoint.getY() + this.endPoint.getY()) / 2;
        return new Point(midPointX, midPointY);
    }

}
// END