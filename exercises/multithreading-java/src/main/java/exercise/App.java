package exercise;

import java.util.Map;
import java.util.logging.Logger;

class App {
    private static final Logger LOGGER = Logger.getLogger("AppLogger");

    // BEGIN
    public static Map<String, Integer> getMinMax(int[] numbs) {
        MaxThread maxThread = new MaxThread(numbs);
        LOGGER.info("Thread " + maxThread.getName() + " started");
        maxThread.start();

        try {
            maxThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LOGGER.info("Thread " + maxThread.getName() + " finished");

        MinThread minThread = new MinThread(numbs);
        minThread.start();
        LOGGER.info("Thread " + minThread.getName() + " started");

        try {
            minThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LOGGER.info("Thread " + minThread.getName() + " finished");

        return Map.of("min", minThread.getMin(),
                "max", maxThread.getMax());
    }
    // END
}

