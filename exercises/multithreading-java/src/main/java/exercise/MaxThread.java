package exercise;

// BEGIN
public class MaxThread extends Thread {

    private final int[] NUMBS;

    private int max;

    public MaxThread(int[] numbs) {
        this.NUMBS = numbs;
    }

    public int getMax() {
        return this.max;
    }

    @Override
    public void run() {
        this.max = this.NUMBS[0];
        for (int numb : this.NUMBS) {
            this.max = Math.max(this.max, numb);
        }
    }

}
// END
