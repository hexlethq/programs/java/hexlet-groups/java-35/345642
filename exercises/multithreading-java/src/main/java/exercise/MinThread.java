package exercise;

// BEGIN
public class MinThread extends Thread {

    private final int[] NUMBS;

    private int min;

    public MinThread(int[] numbs) {
        this.NUMBS = numbs;
    }

    public int getMin() {
        return this.min;
    }

    @Override
    public void run() {
        this.min = this.NUMBS[0];
        for (int numb : this.NUMBS) {
            this.min = Math.min(this.min, numb);
        }
    }

}
// END
