package exercise.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws IOException {
        // BEGIN
        String resourcesPath = Objects.requireNonNull(
                this.getClass().getClassLoader().getResource("users.json")).getPath();
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(resourcesPath), List.class);
        // END
    }

    private void showUsers(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException {

        // BEGIN
        StringBuilder body = new StringBuilder();
        body.append("""
                <html lang="ru">
                    <head>
                    </head>
                    <body>
                """);

        for (Object user : getUsers()) {
            String[] userParams = user.toString()
                    .replace("{", "")
                    .replace("}", "")
                    .split(", ");

            body.append("""
                            <table>
                              <tr>
                              
                    """)
                    .append("            <td>").append(getUserParams(userParams).get("id")).append("</td>")
                    .append("            <td>\n").append("               <a href=\"/users/")
                    .append(getUserParams(userParams).get("id")).append("\">")
                    .append(getUserParams(userParams).get("firstName")).append(" ")
                    .append(getUserParams(userParams).get("lastName")).append("</a>\n")
                    .append("""
                                </td>
                              </tr>
                            </table>
                """);
        }
        body.append("""
                    </body>
                </head>
                """);
        PrintWriter out = response.getWriter();
        out.println(body);
        // END
    }

    private void showUser(HttpServletRequest request,
                         HttpServletResponse response,
                         String id)
                 throws IOException {

        // BEGIN
        StringBuilder body = new StringBuilder();
        body.append("""
                <html lang="ru">
                    <head>
                    </head>
                    <body>
                """);

        for (Object user : getUsers()) {
            String[] userParams = user.toString()
                    .replace("{", "")
                    .replace("}", "")
                    .split(", ");

            if (getUserParams(userParams).get("id").equals(id)) {
                body.append("""
                            <table>
                              <tr>
                              
                    """)
                        .append("            <td>").append(getUserParams(userParams).get("id")).append("</td>\n")
                        .append("            <td>")
                        .append(getUserParams(userParams).get("firstName"))
                        .append(" ")
                        .append(getUserParams(userParams).get("lastName"))
                        .append("</td>\n")
                        .append("            <td>").append(getUserParams(userParams).get("email")).append("</td>\n")
                        .append("""
                              </tr>
                            </table>
                """);
                break;
            }
        }
        body.append("""
                    </body>
                </head>
                """);
        if (body.toString().contains("<table>")) {
            PrintWriter out = response.getWriter();
            out.println(body);
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }

        // END
    }

    private Map<String, String> getUserParams(String[] userParams) throws IOException {
        Map<String, String> params = new HashMap<>();
        Arrays.stream(userParams).forEach(
                param -> {
                    if (param.contains("firstName=")) {
                        params.put("firstName", param.replace("firstName=", ""));
                    }
                    if (param.contains("lastName=")) {
                        params.put("lastName", param.replace("lastName=", ""));
                    }
                    if (param.contains("id=")) {
                        params.put("id", param.replace("id=", ""));
                    }
                    if (param.contains("email=")) {
                        params.put("email", param.replace("email=", ""));
                    }
                }
                );
        return params;
    }

}
