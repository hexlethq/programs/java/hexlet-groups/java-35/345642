package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(double sideOfTriangleA, double sideOfTriangleB, int angleBetweenAandB) {
        return Math.sin(Math.toRadians(angleBetweenAandB)) * sideOfTriangleA * sideOfTriangleB * 0.5;
    }

    public static void main(String[] args) {
        System.out.println(getSquare(4,5,45));
    }
    // END
}
