package exercise;

import java.util.Arrays;

class SafetyList {
    // BEGIN
    private int size = 0;
    private int[] numbs = new int[size];

    public synchronized boolean add(int numb) {
        this.size += 1;
        this.numbs = Arrays.copyOf(this.numbs, this.size);
        this.numbs[this.size - 1] = numb;
        return true;
    }

    public int getSize() {
        return this.numbs.length;
    }

    public int get(int i) {
        return this.size;
    }

    // END
}
