package exercise;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfig {

    // BEGIN
    // Создаём очередь с именем "queue"
    // Так как мы не можем быть уверены, что приложение-источник будет запущено первым
    // Повторяем объявление очереди в приложении-получателе
    @Bean
    Queue queue() {
        return new Queue("queue", false);
    }
    // END
}
