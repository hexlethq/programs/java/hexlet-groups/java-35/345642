package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] intNum) {
        int[] newIntNum = new int[intNum.length];
        int k = 0;
        for (int i = intNum.length - 1; i >= 0; i--) {
            newIntNum[i] = intNum[k];
            k += 1;
        }
        return newIntNum;
    }
    public static int mult(int[] intNum) {
        int multiplication = 1;
        for (int elements : intNum) {
            multiplication = multiplication * elements;
        }
        return multiplication;
    }
    // END
}
