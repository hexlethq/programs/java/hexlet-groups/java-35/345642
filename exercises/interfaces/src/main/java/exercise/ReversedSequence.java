package exercise;

// BEGIN
public class ReversedSequence implements java.lang.CharSequence {

    String str;

    public ReversedSequence(String str) {
        char[] chars = str.toCharArray();
        StringBuilder reverseStr = new StringBuilder();
        for (int i = str.length() - 1; i >= 0; i--) {
            reverseStr.append(chars[i]);
        }
            this.str = reverseStr.toString();
    }

    @Override
    public int length() {
        return str.length();
    }

    @Override
    public char charAt(int i) {
        if (i >= 0 && i < str.length()) {
            char[] chars = str.toCharArray();
            return chars[i];
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public CharSequence subSequence(int beginIndex, int endIndex) {
        if (beginIndex >= 0 && endIndex > 0 && endIndex < str.length() && beginIndex <= endIndex) {
            char[] chars = str.toCharArray();
            StringBuilder subStr = new StringBuilder();
            for (int i = beginIndex; i < endIndex; i++) {
                subStr.append(chars[i]);
            }
            return str = subStr.toString();
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public String toString() {
        return str;
    }

}
// END
