package exercise;

// BEGIN
public interface Home {
    double getArea();
    int compareTO(Home home);
    String toString();
}
// END
