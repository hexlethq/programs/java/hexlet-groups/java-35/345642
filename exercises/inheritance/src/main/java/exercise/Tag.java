package exercise;

import java.util.Map;

// BEGIN
public class Tag {

    public String tagName;
    public Map<String, String> attributes;

    public Tag(String tagName, Map<String, String> attributes) {
        this.tagName = tagName;
        this.attributes = attributes;
    }

    public String toString() {
        StringBuilder tag = new StringBuilder("<" + tagName + " ");
        attributes.forEach(
                (k, v) -> tag.append(k).append("=").append("\"").append(v).append("\" ")
        );
        tag.replace(tag.length() - 1, tag.length(), ">");
        return tag.toString();
    }

}
// END
