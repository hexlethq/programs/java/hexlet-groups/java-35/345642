package exercise;

import java.util.Map;
import java.util.List;

// BEGIN
public class PairedTag extends Tag {

    private String tagBody;
    private List<Tag> tags;

    public PairedTag(String tagName, Map<String, String> attributes, String tagBody, List<Tag> tags) {
        super(tagName, attributes);
        this.tagBody = tagBody;
        this.tags = tags;
    }

    @Override
    public String toString() {
        StringBuilder tag = new StringBuilder();
        tag.append(super.toString()).append(tagBody);
        tags.forEach((t) -> tag.append(t.toString()));
        return tag.append("</").append(tagName).append(">").toString();
    }

}
// END
