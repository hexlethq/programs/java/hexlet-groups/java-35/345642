package exercise;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;

// BEGIN
public class App {

    public static List<Map<String, String>> findWhere(List<Map<String, String>> books, Map<String, String> findBooks) {
        List<Map<String, String>> listOfMap = new ArrayList<>();
        for (Map<String, String> element : books) {
            if (!foundBooks(element, findBooks).isEmpty()) {
                listOfMap.add(foundBooks(element, findBooks));
            }
        }
        return listOfMap;
    }

    private static Map<String, String> foundBooks (
            Map<String, String> mapTitleAuthorAndYear,
            Map<String, String> mapFindBooks) {
        List<String> valuesMapTitleAuthorAndYear = new ArrayList<>(mapTitleAuthorAndYear.values());
        List<String> valuesMapFindBooks = new ArrayList<>(mapFindBooks.values());
        if (valuesMapTitleAuthorAndYear.containsAll(valuesMapFindBooks)) {
            return mapTitleAuthorAndYear;
        } else {
            return new HashMap<>();
        }
    }
}
//END
