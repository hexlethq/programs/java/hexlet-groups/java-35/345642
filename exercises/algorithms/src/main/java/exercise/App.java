package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] arr) {
        int z = 1;
        for (int k = 0; k < arr.length; k++) {
            for (int i = 0; i < arr.length - z; i++) {
                if (arr[i] > arr[i + 1]) {
                    arr[i] = arr[i] + arr[i + 1];
                    arr[i + 1] = arr[i] - arr[i + 1];
                    arr[i] = arr[i] - arr[i + 1];
                }
            } z += 1;
        } return arr;
    }
    // END
}