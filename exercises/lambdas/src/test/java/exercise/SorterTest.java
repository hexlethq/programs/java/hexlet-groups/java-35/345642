package exercise;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;

class SorterTest {
    @Test
    void testTakeOldestMans() {

        List<Map<String, String>> users1 = List.of(
                Map.of("name", "Vladimir Nikolaev", "birthday", "1990-12-27", "gender", "male"),
                Map.of("name", "Andrey Petrov", "birthday", "1989-11-23", "gender", "male"),
                Map.of("name", "Anna Sidorova", "birthday", "1996-09-09", "gender", "female"),
                Map.of("name", "John Smith", "birthday", "1989-03-11", "gender", "male"),
                Map.of("name", "Vanessa Vulf", "birthday", "1985-11-16", "gender", "female"),
                Map.of("name", "Alice Lucas", "birthday", "1986-01-01", "gender", "female"),
                Map.of("name", "Elsa Oscar", "birthday", "1970-03-10", "gender", "female")
        );

        List<String> oldestMans1 = List.of("John Smith", "Andrey Petrov", "Vladimir Nikolaev");

        List<String> actual1 = Sorter.takeOldestMans(users1);
        assertThat(actual1).isEqualTo(oldestMans1);

        List<Map<String, String>> users2 = List.of(
                Map.of("name", "Ivan Tarasov", "birthday", "1990-11-14", "gender", "male"),
                Map.of("name", "Petr Russkih", "birthday", "1949-01-12", "gender", "male"),
                Map.of("name", "Inna Font", "birthday", "1999-09-09", "gender", "female"),
                Map.of("name", "John White", "birthday", "2021-01-01", "gender", "male"),
                Map.of("name", "Victor Wolf", "birthday", "1990-11-14", "gender", "male"),
                Map.of("name", "Alex Wues", "birthday", "1971-12-03", "gender", "male"),
                Map.of("name", "Kirill Suvorov", "birthday", "1982-06-10", "gender", "male")
        );

        List<String> oldestMans2 = List.of("Petr Russkih", "Alex Wues", "Kirill Suvorov",
                "Ivan Tarasov", "Victor Wolf", "John White");

        List<String> actual2 = Sorter.takeOldestMans(users2);
        assertThat(actual2).isEqualTo(oldestMans2);

        List<Map<String, String>> users3 = List.of(
                Map.of("name", "Anna Ivanova", "birthday", "1990-11-14", "gender", "female"),
                Map.of("name", "Olga Tarasova", "birthday", "1949-01-12", "gender", "female"),
                Map.of("name", "Inna Font", "birthday", "1999-09-09", "gender", "female")
        );

        List<String> oldestMans3 = new ArrayList<>();

        List<String> actual3 = Sorter.takeOldestMans(users3);
        assertThat(actual3).isEqualTo(oldestMans3);
    }
}
