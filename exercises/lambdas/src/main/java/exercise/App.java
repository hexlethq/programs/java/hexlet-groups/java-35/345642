package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

public class App {

    public static String[][] enlargeArrayImage(String[][] image) {
        return Arrays.stream(image)
                .flatMap(x -> Stream.of(x, x))
                .map(x -> Arrays.stream(x)
                            .flatMap(x1 -> Stream.of(x1, x1))
                            .toArray(String[]::new))
                .toArray(String[][]::new);
    }

}
