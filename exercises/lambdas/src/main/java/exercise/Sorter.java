package exercise;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.List;

public class Sorter {
    public static List<String> takeOldestMans(List<Map<String, String>> users) {
        List<String> oldestMans = new ArrayList<>();
        users.stream()
                .filter(element -> element.containsValue("male"))
                .sorted(Comparator.comparing(e -> e.get("birthday"))) //аналогично записи .sorted((e1, e2) -> e1.get("birthday").compareTo(e2.get("birthday")))
                .forEach(element -> oldestMans.add(element.get("name")));
        return oldestMans;
    }
}
