package exercise;

import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import com.fasterxml.jackson.databind.ObjectMapper;
// BEGIN
import org.junit.jupiter.api.Test;
import java.util.Map;
import static org.assertj.core.api.Assertions.assertThat;
// END


class FileKVTest {

    private static Path filepath = Paths.get("src/test/resources/file").toAbsolutePath().normalize();

    @BeforeEach
    public void beforeEach() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(new HashMap<String, String>());
        Files.writeString(filepath, content, StandardOpenOption.CREATE);
    }

    // BEGIN
    @Test
    void fileKVTest() {
        KeyValueStorage storage = new FileKV("src/test/resources/file", Map.of("key", "value"));
        String expected1 = "value";
        String result1 = storage.get("key", "default");
        assertThat(result1).isEqualTo(expected1);

        storage.set("key2", "value2");
        Map<String, String> expected2 = Map.of("key", "value", "key2", "value2");
        Map<String, String> result2 = storage.toMap();
        assertThat(result2).isEqualTo(expected2);

        storage.unset("key");
        storage.unset("key2");
        Map<String, String> expected3 = Map.of();
        Map<String, String> result3 = storage.toMap();
        assertThat(result3).isEqualTo(expected3);

        String expected4 = "default";
        String result4 = storage.get("key", "default");
        assertThat(result1).isEqualTo(expected1);

    }
    // END
}
