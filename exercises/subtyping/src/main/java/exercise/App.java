package exercise;

import java.util.Map;

// BEGIN
public class App {

    public static void swapKeyValue(KeyValueStorage memoryKV) {
        InMemoryKV swapMemoryKV = new InMemoryKV(Map.of());
        memoryKV.toMap().forEach(
                (k, v) -> {
                    swapMemoryKV.set(v, k);
                    memoryKV.unset(k);
                }
        );
        swapMemoryKV.toMap().forEach(
                memoryKV::set
        );
    }

}
// END
