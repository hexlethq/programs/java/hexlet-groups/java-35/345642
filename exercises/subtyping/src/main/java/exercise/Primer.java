package exercise;

import java.util.HashMap;
import java.util.Map;

public class Primer {

    public static void main(String[] args) {
        KeyValueStorage storage = new InMemoryKV(Map.of("key", "value"));
        storage.set("key2", "value2");
        System.out.println("before swap: " + storage.toMap());
        App.swapKeyValue(storage);
        System.out.println("after swap: " + storage.toMap());

    }

}
