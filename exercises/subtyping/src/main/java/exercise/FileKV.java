package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class FileKV implements KeyValueStorage {

    private final String path;
    private final Map<String, String> storage = new HashMap<>();

    public FileKV(String path, Map<String, String> storage) {
        this.path = path;
        this.storage.putAll(storage);
        Utils.writeFile(path, Utils.serialize(this.storage));

    }

    @Override
    public void set(String key, String value) {
        storage.put(key, value);
        Utils.writeFile(path, Utils.serialize(storage));
    }

    @Override
    public void unset(String key) {
        storage.putAll(Utils.unserialize(Utils.readFile(path)));
        storage.remove(key);
        Utils.writeFile(path, Utils.serialize(storage));
    }

    @Override
    public String get(String key, String defaultValue) {
        Map<String, String> data = Utils.unserialize(Utils.readFile(path));
        return data.getOrDefault(key, defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        return Utils.unserialize(Utils.readFile(path));
    }
}
// END
