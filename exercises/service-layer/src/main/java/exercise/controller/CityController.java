package exercise.controller;
import com.fasterxml.jackson.core.JsonProcessingException;
import exercise.CityNotFoundException;
import exercise.model.City;

import exercise.model.Weather;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;


@RestController
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherService weatherService;

    // BEGIN
    @GetMapping("/cities/{id}")
    public Weather getCitiesById(@PathVariable long id) throws JsonProcessingException {

        City city = cityRepository.findById(id)
                .orElseThrow(() -> new CityNotFoundException("City not found"));

        return weatherService.getCityWeather(city);
    }

    @GetMapping("/search")
    public List<Map<String, String>> getTemperatureByCityName(@RequestParam(required = false) String name)
            throws JsonProcessingException {

        List<City> cities;
        if (name == null) {
            cities = cityRepository.findAllByOrderByName();
        } else {
            cities = cityRepository.findAllByNameStartingWithIgnoreCase(name);
        }

        return cities.stream()
                .map(city -> {
                    try {
                        return weatherService.getCityWeather(city);
                    } catch (JsonProcessingException e) {
                        throw new CityNotFoundException("City not found");
                    }
                })
                .map(weather -> Map.of(
                        "temperature", Integer.toString(weather.getTemperature()),
                        "name", weather.getName()))
                .collect(Collectors.toList());
    }
    // END
}

