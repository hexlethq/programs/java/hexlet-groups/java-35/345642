package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(double sideA, double sideB, double sideC) {
        if (sideA + sideB > sideC && sideA + sideC > sideB && sideB + sideC > sideA) {
            if (sideA == sideB && sideA == sideC) {
                return "Равносторонний";
            } else if (sideA != sideB && sideA != sideC && sideB != sideC) {
                return "Разносторонний";
            } else {
                return "Равнобедренный";
            }
        } else {
            return "Треугольник не существует";
        }
    }
    // END
}
