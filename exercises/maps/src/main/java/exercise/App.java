package exercise;

import java.util.HashMap;
import java.util.Map;

public class App {

    public static Map<String, Integer> getWordCount(String sentence) {
        Map<String, Integer> wordAndCount = new HashMap<>();
        if(sentence.equals("")) {
            return wordAndCount;
        }
        String[] sentenceSplit = sentence.split(" ");
        for (String str : sentenceSplit) {
            int count = 1;
            count += wordAndCount.getOrDefault(str, 0);
            wordAndCount.put(str, count);
        }
        return wordAndCount;
    }

    public static String toString(Map<String, Integer> map) {
        if(map.isEmpty()) {
            return "{}";
        }
        StringBuilder str = new StringBuilder("{\n");
        String twoSpace = "  ";
        for(Map.Entry<String, Integer> entry : map.entrySet()) {
            str.append(twoSpace).append(entry).append("\n");
        }
        str.append("}");
        return str.toString().replace("=", ": ");
    }
}
